(function () {
    "use strict";
    angular.module("MyApp").controller("UsersCtrl", UsersCtrl);

    UsersCtrl.$inject = ["MyAppService", "$state"];

    function UsersCtrl(MyAppService, $state) {
        // console.log("BooksCtrl");
        var self = this; // vm
        self.users = [];

        self.editUser = function (id) {
            // console.log(id);
            $state.go("edit", { id: id });
        }

        self.init = function () {
            MyAppService.getUsers()
                .then(function (result) {
                    // console.log(result);
                    self.users = result;
                }).catch(function (err) {
                    console.log(err);
                });
        }

        self.init();


    }

})();