const fs = require("fs");
const path = require("path");
const multer = require("multer");
const aws = require("aws-sdk");
const multerS3 = require("multer-s3");
const storage = multer.diskStorage({
    destination: "./server/upload_tmp",
    filename: function (req, file, callback) {
        console.log(file);
        callback(null, Date.now() + "-" + file.originalname)
    }
});
const upload = multer({
    storage: storage
})

aws.config.region = "ap-southeast-1";
console.log("AWS bucket init...");
var s3 = new aws.S3();

var uploadS3 = new multer({
    storage: multerS3({
        s3: s3,
        bucket: "nus-iss-fsf",
        metadata: function (req, file, cb) {
            console.log("file.fieldname: " + file.fieldname);
            cb(null, { fieldName: file.fieldname });
        },
        key: function (req, file, cb) {
            cb(null, Date.now() + "-" + file.originalname);
        }
    })
})

module.exports = function (app) {
    app.post("/upload", upload.single("img-file"), function (req, res) {
        console.log("upload");
        fs.readFile(req.file.path, function (err, data) {
            if (err) {
                console.log("Error: " + err);
            }

            res.status(202).json({ size: req.file.size });
        })
    });

    app.post("/uploadS3", uploadS3.single("img-file"), function (req, res) {
        console.log("uploadS3");
        res.status(202).send("File sent successfully");
    });
}